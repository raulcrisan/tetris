var Tetris = require('../tetris');
var helper = require('../tetris-helper');
var fs = require('fs');

var games = helper.loadGames('../app/games.txt');
var solutions = ['game,moves'];
var totalScore = 0;
var GRID_W = 10;
var GRID_H = 20;

// heuristics
var aggH = -0.510066,
    compL = 0.760666,
    holes = -0.35663,
    bump = -0.184483;


function playGame(game, gameId) {
    var moves = [];
    var shapes = game.split('');

    var t = new Tetris(game);

    shapes.forEach(function(shape, index) {

        var nextShape,
            secondNextShape;

        if (index < shapes.length - 1) {
            nextShape = shapes[index+1];
        }
        /*if (index < shapes.length - 2) {
            secondNextShape = shapes[index+2];
        }*/
        var position = getBestPosition(t, shape, nextShape, secondNextShape);
        t.makeMove(position.col, position.rot);

        var move = position.col + ':' + position.rot;
        moves.push(move);
    });

    moves = moves.join(';');

    var score = helper.getScore(game, moves);
    console.log('Game: ' + gameId + ' Score: ' + score);
    totalScore += score;

    var solution = gameId + ',' + moves;
    solutions.push(solution);

    writeSolution(gameId, moves, score);
}

function getBestPosition(tetris, shape, nextShape, secondNextShape) {
    var position = {'col': 0, 'rot': 0};
    var bestScore = -1000000;
    var maxRot = 4;
    if (shape === 'O') {
        maxRot = 1;
    }
    if (shape === 'I') {
        maxRot = 2;
    }

    for(var rot = 0; rot < maxRot; rot++ ) {
        var offset = getOffset(shape, rot);

        for(var jj = 0 - offset; jj< GRID_W - offset; jj++) {
            var t = tetris.clone();
            t.simulateMove(jj, rot);

            var score;
            if (!t.lost) {
                score = t.getFitness(aggH, compL, holes, bump);
                // score = t.getHeuristicScore(aggH, compL, holes, bump);
                // score = t.getCompleteLinesScore(compL);

               if (nextShape) {
                    score += getNextBestScore(t, nextShape, secondNextShape);
                } /*else {
                    score += score = t.getHeuristicScore(aggH, compL, holes, bump);
                }*/

                if (score > bestScore) {
                    bestScore = score;
                    position.col = jj;
                    position.rot = rot;
                }
            }
        }
    }

    return position;
}

function getNextBestScore(tetris, shape, nextShape) {
    var bestScore = -1000000;
    var maxRot = 4;
    if (shape === 'O') {
        maxRot = 1;
    }
    if (shape === 'I') {
        maxRot = 2;
    }

    for(var rot = 0; rot < maxRot; rot++ ) {
        var offset = getOffset(shape, rot);

        for(var jj = 0 - offset; jj< GRID_W - offset; jj++) {
            var t = tetris.clone();
            t.simulateMove(jj, rot);

            var score;
            if (!t.lost) {
                score = t.getFitness(aggH, compL, holes, bump);
                // score = t.getCompleteLinesScore(compL);

                if (nextShape) {
                    score += getSecondNextBestScore(t, nextShape);
                } /*else {
                    score += score = t.getHeuristicScore(aggH, compL, holes, bump);
                }*/

                if (score > bestScore) {
                    bestScore = score;
                }
            }
        }
    }

    return bestScore;
}

function getSecondNextBestScore(tetris, shape) {
    var bestScore = -1000000;
    var maxRot = 4;
    if (shape === 'O') {
        maxRot = 1;
    }
    if (shape === 'I') {
        maxRot = 2;
    }

    for(var rot = 0; rot < maxRot; rot++ ) {
        var offset = getOffset(shape, rot);

        for(var jj = 0 - offset; jj< GRID_W - offset; jj++) {
            var t = tetris.clone();
            t.simulateMove(jj, rot);

            var score;
            if (!t.lost) {
                score = t.getFitness(aggH, compL, holes, bump);
                // score = t.getCompleteLinesScore(compL);
                // score += t.getHeuristicScore(aggH, compL, holes, bump);
                if (score > bestScore) {
                    bestScore = score;
                }
            }
        }
    }

    return bestScore;
}

function getOffset(shape, rot) {
    switch (shape) {
        case 'I': {
            if (rot == 1) {
                return 2;
            }
            if (rot == 3) {
                return 1;
            }
        }
        case 'L':
        case 'J':
        case 'S':
        case 'Z':
        case 'T': {
            if (rot == 1) {
                return 1;
            }
        }
        default: return 0
    }
}
//var arr = [40,41,44,45,46];
//var arr = [47,48,52,53,57];
//var arr = [42,43,49,50,51];
//var arr = [54,55,56,58,59];
games.forEach(function(game, gameId) {
    //if (arr.indexOf(gameId) != -1) {
        playGame(game, gameId);
    //}
});

function writeSolution(gameId, moves, score) {
    var outputDir = './solutions_h8/';
    var outputFile = outputDir + 'solution-' + gameId + '-' + score + '-' + new Date().getTime() + '.csv';
    var solution = gameId + ',' + moves;

    if (!fs.existsSync(outputDir)) {
        fs.mkdirSync(outputDir);
    }
    fs.writeFileSync(outputFile, solution, function(err) {
        if (err) {
            return console.log(err);
        }

        console.log('Saved solutions in ' + outputFile + '\nGame Score: ' + score);
    });
}

var outputDir = './solutions/';
var outputFile = outputDir + 'solution-' + totalScore + '-' + new Date().getTime() + '.csv';

if (!fs.existsSync(outputDir)) {
    fs.mkdirSync(outputDir);
}
fs.writeFile(outputFile, solutions.join('\n'), function(err) {
    if (err) {
        return console.log(err);
    }

    console.log('Saved solutions in ' + outputFile + '\nTotal Score: ' + totalScore);
});